<?php
ini_set('display_errors', 'On');
define('ISITSAFETORUN', TRUE); 

require 'mydatabase.php';

$db = mysqli_connect($hostname, $username, $password) or die( "Unable to connect to MySQL");
$selected = mysqli_select_db($db, $mydatabase) or die("Unable to connect to $mydatabase");

$sql = "SELECT * FROM PYPLdata ORDER BY share DESC";

$result = mysqli_query($db, $sql) or die ("Could not action the query $sql");

$data = array();
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    array_push($data, $row);
}

mysqli_close($db);


?>

<!doctype html>
<html lang="en">
    <head>
        <title>PHP demo: step 3, database</title>
        <style type="text/css">
            body {
            font-family: sans-serif;
            }

            table th, table td {
                border: 1px solid black;
                padding: 2px 4px 2px 4px;
            }
        </style>
    </head>
    <body>
        <h1>PHP demo - step 3, table created from database</h1>

        <table>
            <tr>
                <th>Rank</th>
                <th>Language</th>
                <th>Share</th>
                <th>Trend (yearly)</th>
            </tr>
            <?php
            
            $rank = 1;
            foreach ($data as $row) {
                echo '<tr>';
                echo "<td>$rank</td>";
                echo '<td>' . htmlspecialchars($row['language']) . '</td>';
                echo '<td>' . sprintf("%0.2f", $row['share']) . '%</td>';
                echo '<td>' . sprintf("%+0.1f", $row['trend']) . '%</td>';
                echo '</tr>';
                $rank++;
            }
            ?>
        </table>

        <p>Source: <a href="http://pypl.github.io/PYPL.html">PopularitY of Programming Language Index</a> (PYPL),
        which is based on Google searches for tutorials.</p>
    </body>
</html>

