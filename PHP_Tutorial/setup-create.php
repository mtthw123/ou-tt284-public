<?php

define('ISITSAFETORUN', TRUE); 

require('mydatabase.php');

$db = mysqli_connect( $hostname, $username, $password ) 
    or die( "Unable to connect to MySQL");

echo "<p>Connected to MySQL</p>";

//select a database to work with
$selected = mysqli_select_db(  $db, $mydatabase ) 
    or die("Unable to connect to " . $mydatabase );

echo "<p>Connected to MySQL database {$mydatabase}</p>";

$thisquery = "SHOW TABLES FROM " . $mydatabase ;// This is the SQL instruction
$result = mysqli_query( $db, $thisquery ) 
    or die (" Could not action the query " . $thisquery );
while ($row = mysqli_fetch_array($result)) {
	echo "<p>Table: {$row[0]}</p>"; //note that there is only one item in each row, so the first item is item zero
}
?>

<p>Now create our own table for testing. </p>

<?php   
// sql to create table
$sqlCreate = 
    "CREATE TABLE IF NOT EXISTS PYPLdata (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
language VARCHAR(30) NOT NULL,
share DECIMAL(5, 2) NOT NULL,
trend DECIMAL(5, 2) NOT NULL,
logo VARCHAR(255)
)";

if (mysqli_query($db, $sqlCreate)) {
    echo "<p>Table PYPLdata created successfully, or already exists<p>";
} else {
    echo "<p>Error creating table: " . mysqli_error($db) . "</p>";
    exit;
}

$sqlDelete = "DELETE FROM PYPLdata";
$statement = mysqli_prepare($db, $sqlDelete);
$result = mysqli_stmt_execute($statement);
echo "<p>Cleared PYPLdata table</p>";
mysqli_stmt_close($statement);

$data = array(
    array('language' => 'Python', 
          'share' => 30.44, 
          'trend' => 1.2,
          'logo' => 'logo-python.png'),
    array('language' => 'Java', 
          'share' => 16.76, 
          'trend' => -2.0,
          'logo' => 'logo-java.jpeg'),
    array('language' => 'JavaScript', 
          'share' => 8.44, 
          'trend' => 0.3,
          'logo' => 'logo-javascript.png'),
    array('language' => 'C#', 
          'share' => 6.53, 
          'trend' => -0.7,
          'logo' => 'logo-csharp.png'),
    array('language' => 'C/C++', 
          'share' => 6.33, 
          'trend' => 0.3,
          'logo' => 'logo-cpp.png'),
    array('language' => 'PHP', 
          'share' => 6.05, 
          'trend' => -0.2,
          'logo' => 'logo-php.png'),
    array('language' => 'R', 
          'share' => 3.87, 
          'trend' => 0.1,
          'logo' => 'logo-r.png'),
);

$sqlInsert = "INSERT INTO PYPLdata (language, share, trend, logo) VALUES (?, ?, ?, ?)";
$statement = mysqli_prepare($db, $sqlInsert);

foreach($data as $row) {
    mysqli_stmt_bind_param(
        $statement,
        'sdds',
        $row['language'],
        $row['share'],
        $row['trend'],
        $row['logo']);
    mysqli_stmt_execute($statement);
}
mysqli_stmt_close($statement);

echo "<p>Populated PYPLdata table</p>";


mysqli_close($db);
?>
